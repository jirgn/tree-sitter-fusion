#include <tree_sitter/parser.h>
#include <wctype.h>

enum TokenType {
  AFX_COMMENT,
};

void *tree_sitter_fusion_external_scanner_create() { return NULL; }
void tree_sitter_fusion_external_scanner_destroy(void *p) {}
void tree_sitter_fusion_external_scanner_reset(void *p) {}
unsigned tree_sitter_fusion_external_scanner_serialize(void *p, char *buffer) { return 0; }
void tree_sitter_fusion_external_scanner_deserialize(void *p, const char *b, unsigned n) {}

static void advance(TSLexer *lexer) { lexer->advance(lexer, false); }
static void skip(TSLexer *lexer) { lexer->advance(lexer, true); }
static void mark_end(TSLexer *lexer) { lexer->mark_end(lexer); }

static bool scan_afx_comment(TSLexer *lexer) {
    if (lexer->lookahead != '<') return false;
    advance(lexer);
    if (lexer->lookahead != '!') return false;
    advance(lexer);
    if (lexer->lookahead != '-') return false;
    advance(lexer);
    if (lexer->lookahead != '-') return false;
    advance(lexer);

    unsigned dashes = 0;
    while (lexer->lookahead) {
      switch (lexer->lookahead) {
        case '-':
          ++dashes;
          break;
        case '>':
          if (dashes >= 2) {
            lexer->result_symbol = AFX_COMMENT;
            advance(lexer);
            mark_end(lexer);
            return true;
          }
        default:
          dashes = 0;
      }
      advance(lexer);
    }
    return false;
}

bool tree_sitter_fusion_external_scanner_scan(void *payload, TSLexer *lexer, const bool *valid_symbols) {
  if(valid_symbols[AFX_COMMENT]) {
    return scan_afx_comment(lexer);
  }
  return false;
}

